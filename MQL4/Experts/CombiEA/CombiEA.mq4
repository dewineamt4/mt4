//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2012, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+

#include "..\..\Include\CombiEA\Util\Order.mqh"
#include "..\..\Include\CombiEA\IndicatorFetcher.mqh"
#include "..\..\Include\CombiEA\StrategyRunner.mqh"

extern double Lots         =0.5;
extern double TrailingStop =30;
extern int Slippage        =3;
extern int Magic           =16384;

UtilOrder *utilOrder;
IndicatorFetcher *iData;
StrategyRunner *strategyRunner;
   
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {
   utilOrder      =new UtilOrder(Lots, Slippage, Magic);
   iData          =new IndicatorFetcher();
   strategyRunner =new StrategyRunner(iData, utilOrder);
   
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int deinit()
  {
   delete utilOrder;
   delete iData;
   delete strategyRunner;

   return(0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

int start()
  {

// initial data checks
   if(Bars<100)
     {
      Print("bars less than 100");
      return(0);
     }

// Let the custom indicators dore there work
   iData.tick();
   
// Let the strategies do there work     
   strategyRunner.tick();

   return(0);
  }