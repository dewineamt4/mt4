//+------------------------------------------------------------------+
//|                                                     MA_Shade.mq4 |
//|                                                           oromek |
//|                                                oromeks@gmail.com |
//+------------------------------------------------------------------+
#property copyright "oromek"
#property link      "oromeks@gmail.com"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 Purple

extern int MA1_Period=21;
extern int MA2_Period=50;

extern int TrendCheckLength;

int DIR_UP=1;
int DIR_DOWN=-1;

double MA1,MA2;
double MA1_Buffer[],MA2_Buffer[];

//+------------------------------------------------------------------+
//| Public return values 
//+------------------------------------------------------------------+
double TrendDirectionBuffer[]; // Stores the side of the current price (1 = above average, 0 = below average)
double TrendStrengthBuffer[];
double TrendSwitchBuffer[]; // Stores if the bar made a switchs (1 = true, 0 = false)
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   SetIndexStyle(0,DRAW_HISTOGRAM,STYLE_SOLID,1);
   SetIndexStyle(1,DRAW_HISTOGRAM,STYLE_SOLID,1);

   SetIndexBuffer(0,MA1_Buffer);
   SetIndexBuffer(1,MA2_Buffer);

   SetIndexBuffer(2,TrendDirectionBuffer);
   SetIndexBuffer(3,TrendStrengthBuffer);
   SetIndexBuffer(4,TrendSwitchBuffer);

   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----

//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int counter1 = 0;
int counter2 = 0;
int counter3 = 0;
int start()
  {
   counter1++;
   
   double trendSwitch;
   
   int counted_bars=IndicatorCounted(),
       limit;
 
   if(counted_bars>0)
      counted_bars--;
   
   limit=Bars-counted_bars;
 
   for(int i=0;i<Bars;i++)
   {
      
      MA1=iMA(NULL,0,MA1_Period,0,MODE_EMA,PRICE_CLOSE,i);
      MA2=iMA(NULL,0,MA2_Period,0,MODE_EMA,PRICE_CLOSE,i);
      
      MA1_Buffer[i]=MA1;
      MA2_Buffer[i]=MA2;

      double currentDifference=iOpen(Symbol(),NULL,i)-MA2;
      double prevDifference =  iOpen(Symbol(),NULL,i+1) - iMA(NULL,0,MA2_Period,0,MODE_SMA,PRICE_CLOSE,i+1);

      //double prevDifference=TrendStrengthBuffer[i-1];
      //Alert("TEST = ", prevDifference);
      trendSwitch=0;

      // Above MA
      if(currentDifference>0) 
        {

         TrendDirectionBuffer[i]=DIR_UP;

         if(prevDifference<0) 
           {
            drawArrow(DIR_UP,i);
            trendSwitch=1;
           }

        }
      // Below MA
      else 
        {
         TrendDirectionBuffer[i]=DIR_DOWN;

         if(prevDifference>0) 
           {
            drawArrow(DIR_DOWN,i);
            trendSwitch=1;
           }
        }

      TrendStrengthBuffer[i]=currentDifference; // ensure positive number
      TrendSwitchBuffer[i]=trendSwitch;
      //calculateTrend(i);
      
      counter3++;
      //Alert("counter3 = ", counter3);
     }

   return(0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void calculateTrend(int i) 
  {
   //int length=ArraySize(TrendSwitchBuffer);
   //int max=TrendCheckLength;

//while(
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int arrowCounter=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawArrow(double dir,int bar)
  {

//bar = 0;
   if(dir==DIR_UP)
     {
      ObjectCreate("bar"+arrowCounter,OBJ_ARROW,0,Time[bar],High[bar]+20*Point); //draw an up arrow
      ObjectSet("bar"+arrowCounter,OBJPROP_STYLE,STYLE_SOLID);
      ObjectSet("bar"+arrowCounter,OBJPROP_ARROWCODE,233);
      ObjectSet("bar"+arrowCounter,OBJPROP_COLOR,Red);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      ObjectCreate("bar"+arrowCounter,OBJ_ARROW,0,Time[bar],Low[bar]-20*Point); //draw a dn arrow
      ObjectSet("bar"+arrowCounter,OBJPROP_STYLE,STYLE_SOLID);
      ObjectSet("bar"+arrowCounter,OBJPROP_ARROWCODE,234);
      ObjectSet("bar"+arrowCounter,OBJPROP_COLOR,Blue);
     }

   arrowCounter++;
  }
//+------------------------------------------------------------------+
