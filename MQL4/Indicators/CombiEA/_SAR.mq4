//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2012, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property indicator_chart_window
#property indicator_buffers 2

extern int MA1_Period=21;
extern int MA2_Period=50;

extern double Step=0.02;
extern double Maximum=0.2;
extern double TimeFrame=0;

double SarBuffer[];
double SarSwitchBuffer[];
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   SetIndexBuffer(0,SarBuffer);
   SetIndexBuffer(1,SarSwitchBuffer);

   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int counted_bars=IndicatorCounted();
   if(counted_bars>0) counted_bars--;
   int limit=Bars-counted_bars;

   for(int i=0; i<limit; i++)
     {
      SarSwitchBuffer[i]=psar(i);
     }

   return(0);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int psar(int i)
  {
   double price=iClose(NULL,NULL,i);
   double sar=iSAR(NULL,TimeFrame,Step,Maximum,i);
   SarBuffer[i]=sar;
   DrawDot(i,sar);

   if((price*1)>sar)
     {
      return(1);
        } else if(price<(sar*1)) {
      return(0);
        } else {
      return(EMPTY_VALUE);
     }
  }
//+------------------------------------------------------------------+

void DrawDot(int offset,double position)
  {
   string objName="Bullseye"+iTime(NULL,0,offset);
   ObjectCreate(objName,OBJ_TEXT,0,Time[offset],position);
   ObjectSetText(objName,CharToStr(159),14,"Wingdings",White);
  }
//+------------------------------------------------------------------+
