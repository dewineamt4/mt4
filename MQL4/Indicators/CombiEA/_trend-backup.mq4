
//+------------------------------------------------------------------+
//|                                                     MA_Shade.mq4 |
//|                                                           oromek |
//|                                                oromeks@gmail.com |
//+------------------------------------------------------------------+
#property copyright "oromek"
#property link      "oromeks@gmail.com"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 Blue
#property indicator_color2 Pink

extern int MA1_Period=21;
extern int MA2_Period=50;

int DIR_UP = 1;
int DIR_DOWN = -1;

double MA1, MA2;
double MA1_Buffer[], MA2_Buffer[];

double TrendBuffer[];
double TrendSwitchBuffer[];

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   SetIndexStyle(0,DRAW_HISTOGRAM,1,1,Red);
   SetIndexStyle(1,DRAW_HISTOGRAM,1,1,Purple);
   SetIndexStyle(2,DRAW_LINE,0,0,White);
   
   //SetIndexStyle(0,DRAW_HISTOGRAM,STYLE_SOLID);
   SetIndexBuffer(0,MA1_Buffer);
   
   //SetIndexStyle(1,DRAW_HISTOGRAM,STYLE_SOLID);
   SetIndexBuffer(1,MA2_Buffer);
   
   SetIndexBuffer(2,TrendBuffer);
   SetIndexBuffer(3,TrendSwitchBuffer);
   
//---- indicators
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
//----
   
//----
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int    counted_bars=IndicatorCounted();
   
   int limit=Bars-counted_bars;
   if(counted_bars>0) limit++;
   
   for(int i=0; i<limit; i++)
   {
      MA1=iMA(NULL,0,MA1_Period,0,MODE_SMA,PRICE_CLOSE,i);
      //MA2=iMA(NULL,0,MA2_Period,0,MODE_SMA,PRICE_CLOSE,i);
      MA2= iBands(Symbol(),0,20,2,0,PRICE_CLOSE,0,i);
      
      MA1_Buffer[i]=MA1;
      MA2_Buffer[i]=MA2;
      
      double currentDifference = iClose(Symbol(),NULL,i) - MA2;
      double prevDifference = iClose(Symbol(),NULL,i+1) - iBands(Symbol(),0,20,2,0,PRICE_CLOSE,0,i+1);
      
      double trendSwitch = 0;
      
      // Trend is going up
      if (currentDifference > 0) {
         TrendBuffer[i]= DIR_UP;
         
         if (prevDifference < 0) {
            drawArrow(DIR_UP, i);
            trendSwitch = 1;
         }
         
         
      }
      // Trend is going down
      else {
         TrendBuffer[i]= DIR_DOWN;
         
         if (prevDifference > 0) {
            drawArrow(DIR_DOWN, i);
            trendSwitch = 1;
         }
      }
      
      TrendSwitchBuffer[i] = trendSwitch;
   }
   return(0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int arrowCounter=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawArrow(double dir,int bar)
  {
   //bar = 0;
   if(dir==DIR_UP)
     {
      ObjectCreate("bar"+arrowCounter,OBJ_ARROW,0,Time[bar],High[bar]+20*Point); //draw an up arrow
      ObjectSet("bar"+arrowCounter,OBJPROP_STYLE,STYLE_SOLID);
      ObjectSet("bar"+arrowCounter,OBJPROP_ARROWCODE,233);
      ObjectSet("bar"+arrowCounter,OBJPROP_COLOR,Red);
     }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
   else
     {
      ObjectCreate("bar"+arrowCounter,OBJ_ARROW,0,Time[bar],Low[bar]-20*Point); //draw a dn arrow
      ObjectSet("bar"+arrowCounter,OBJPROP_STYLE,STYLE_SOLID);
      ObjectSet("bar"+arrowCounter,OBJPROP_ARROWCODE,234);
      ObjectSet("bar"+arrowCounter,OBJPROP_COLOR,Blue);
     }

   arrowCounter++;
  }
//+------------------------------------------------------------------+
