//+------------------------------------------------------------------+
//|                                                     MA_Shade.mq4 |
//|                                                           oromek |
//|                                                oromeks@gmail.com |
//+------------------------------------------------------------------+
#property copyright "oromek"
#property link      "oromeks@gmail.com"
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Red
#property indicator_color2 Purple
#property indicator_color2 Yellow

//+------------------------------------------------------------------+
//| Public return values 
//+------------------------------------------------------------------+
double lipsBuffer[];
double teethBuffer[];
double jawBuffer[];


double TrendDirectionBuffer[]; // Stores the side of the current price (1 = above average, 0 = below average)
double TrendStrengthBuffer[];
double TrendSwitchBuffer[]; // Stores if the bar made a switchs (1 = true, 0 = false)
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
  {
   SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,1);
   SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,1);
   SetIndexStyle(2,DRAW_LINE,STYLE_SOLID,1);
   
   SetIndexBuffer(0,lipsBuffer);
   SetIndexBuffer(1,teethBuffer);
   SetIndexBuffer(2,jawBuffer);

   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator deinitialization function                       |
//+------------------------------------------------------------------+
int deinit()
  {
   return(0);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {

   int counted_bars=IndicatorCounted();
   if(counted_bars>0) counted_bars--;
   int limit=Bars-counted_bars;

   for(int i=0; i<limit; i++)
     {
      //if (Volume[0]==1) return;
      
      double lips = iAlligator(NULL,0,13,3,8,0,5,-2,MODE_SMMA,PRICE_MEDIAN,MODE_GATORLIPS,0);
      double teeth = iAlligator(NULL,0,13,3,8,0,5,-2,MODE_SMMA,PRICE_MEDIAN,MODE_GATORTEETH,0);
      double jaw = iAlligator(NULL,0,13,3,8,0,5,-2,MODE_SMMA,PRICE_MEDIAN,MODE_GATORJAW,0);
      
      lipsBuffer[i] = lips;
      teethBuffer[i] = teeth;
      jawBuffer[i] = jaw;
     }

   return(0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
