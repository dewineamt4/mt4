//+------------------------------------------------------------------+
//|                                              BollingerSqueze.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#property indicator_chart_window
#property indicator_buffers 4
#property indicator_color1 Yellow
#property indicator_color2 Yellow
#property indicator_color3 Yellow      

#include "..\..\Include\CombiEA\Util\Array.mqh"
//#include "..\libraries\hash.mqh"

int squezeWaveLength=250;

double IndicateurBolH;
double IndicateurBolM;
double IndicateurBolM1;
double IndicateurBolL;
double sl=0;

double LineBuf_H[];
double LineBuf_M[];
double LineBuf_L[];

// The position inside the bollinger bands, in percentages. 
// 100% is top at line, 0% is at middle, -100% is at bottom line
double RelativePositionInsideBolligerPerc[];

double BolSpaceHistory[]; // Dynamic Array (http://docs.mql4.com/array/arrayisdynamic)
double BolSpaceAverage;

static int BARS;
//+------------------------------------------------------------------+
//| NewBar function                                                  |
//+------------------------------------------------------------------+
bool IsNewBar()
  {
   if(BARS!=Bars(_Symbol,_Period))
     {
      BARS=Bars(_Symbol,_Period);
      return(true);
     }
   return(false);
  }
  
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setHeaderText(double value)
  {
   ObjectCreate("headerText",OBJ_LABEL,0,0,0,0,0);

   ObjectSet("headerText",OBJPROP_XDISTANCE,30);
   ObjectSet("headerText",OBJPROP_YDISTANCE,30);

   ObjectSetText("headerText","Squeze: "+value,12,"Tahoma",Gold);
  }
//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int OnInit()
  {

   ArraySetAsSeries(BolSpaceHistory,true);

   SetIndexBuffer(0,LineBuf_H); // Upper bollinger line
   SetIndexBuffer(1,LineBuf_M); // Middle bollinger line
   SetIndexBuffer(2,LineBuf_L); // Lower bollinger line
   SetIndexBuffer(3,RelativePositionInsideBolligerPerc);

   SetIndexStyle(0,DRAW_LINE,STYLE_SOLID,1);
   SetIndexStyle(1,DRAW_LINE,STYLE_SOLID,1);
   SetIndexStyle(2,DRAW_LINE,STYLE_SOLID,1);
//---
   return(INIT_SUCCEEDED);
  }
//+------------------------------------------------------------------+
//| Custom indicator iteration function                              |
//+------------------------------------------------------------------+
int start()
  {
   int counted_bars=IndicatorCounted();
   if(counted_bars>0) counted_bars--;
   int limit=Bars-counted_bars;

   for(int i=0; i<limit; i++)
     {
      LineBuf_H[i]=iBands(Symbol(),0,20,2,0,PRICE_CLOSE,MODE_UPPER,i);
      LineBuf_M[i]=iBands(Symbol(),0,20,2,0,PRICE_CLOSE,MODE_MAIN,i);
      LineBuf_L[i]=iBands(Symbol(),0,20,2,0,PRICE_CLOSE,MODE_LOWER,i);

      updateBolSqueze(i);
      setRelativePositionInsideBolligerPerc(i);
     }
//--------------------------------------------------------------------

   if(IsNewBar()==true)
     {
      //updateBolSqueze();

     }

//--- return value of prev_calculated for next call
   return(0);
  }
//+------------------------------------------------------------------+

// Add current space to historyArray, and clean up max-length if necessary.
int counter=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int squezeCounter=0;
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void updateBolSqueze(int i)
  {
/*
   double currentSpace=LineBuf_H[i]-LineBuf_L[i];

   prependToReversedDoubleArray(BolSpaceHistory,currentSpace,squezeWaveLength);

   int currentLength=ArraySize(BolSpaceHistory);

// Calculate the average space
   double total=0;
   for(int i=0; i<currentLength; i++)
     {
      total=total+BolSpaceHistory[i];
     }

// Set the global BolSpaceAverage for external usage.
   BolSpaceAverage=total/currentLength;

// The current offset, relative to the average, in percentage.
   double currentBolSpace=(currentSpace/BolSpaceAverage)*100;

   if(currentBolSpace<30)
     {
      squezeCounter++;

      ObjectCreate("ellipse"+squezeCounter,OBJ_ELLIPSE,0,Time[10],Low[10],Time[0],High[i]);
      ObjectSet("ellipse"+squezeCounter,OBJPROP_SCALE,1.0);
      ObjectSet("ellipse"+squezeCounter,OBJPROP_COLOR,Purple);
     }
   */
//setHeaderText(currentBolSpace);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void setRelativePositionInsideBolligerPerc(int i)
  {
   double totalSpace=(LineBuf_H[i]-LineBuf_L[i]); // The total space between the bollinger bands
   double midSpace=totalSpace/2; // The center of the 'room' line (this price would mean 0%)
   double diffFromMiddle=Bid-LineBuf_M[i]; // The difference from the middle line (can be postive or negative)

   double percentageDiffFromMiddle=0;
//Alert("midSpace=",diffFromMiddle);
// Ensure the number is positive and look how much the 'rejection' of the middle line is
   if(diffFromMiddle==0 || midSpace==0) 
     {
      percentageDiffFromMiddle=0;
        } else {
      percentageDiffFromMiddle=NormalizeDouble((MathAbs(diffFromMiddle)/midSpace)*100,2);
     }

// Return back to negative if nessacery
   if(diffFromMiddle<0)
      percentageDiffFromMiddle=-percentageDiffFromMiddle;

   RelativePositionInsideBolligerPerc[i]=percentageDiffFromMiddle;
  }
//+------------------------------------------------------------------+
