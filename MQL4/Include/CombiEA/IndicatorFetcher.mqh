//+------------------------------------------------------------------+
//|                                                      ProjectName |
//|                                      Copyright 2012, CompanyName |
//|                                       http://www.companyname.net |
//+------------------------------------------------------------------+
#property strict
#include "..\CombiEA\Util\Array.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


class IndicatorsData
  {
public:
   double            MADirection;
   double            MAStrength;
   double            MASwitch;
   double            BollingerPosition;
   double            BollingerSpace;
   double            TrendUp;
   double            TrendDown;
   double            SarPoint;
   
   double            AlligatorLips;
   double            AlligatorTeeth;
   double            AlligatorJaws;
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+


class IndicatorFetcher
  {

   //+------------------------------------------------------------------+
   //|                                                                  |
   //+------------------------------------------------------------------+
private:

   void storeData(IndicatorsData *newData)
     {
      int maxLength = 1000;
      
      int i=ArrayResize(data,ArraySize(data)+1);

      data[i-1]=newData;
      
      if (i > maxLength) {
         ArraySetAsSeries(data, true);
         ArrayResize(data,maxLength);
         ArraySetAsSeries(data, false);
      }
         
     };

   //+------------------------------------------------------------------+
   //|                                                                  |
   //+------------------------------------------------------------------+
public:
                     IndicatorFetcher();
                    ~IndicatorFetcher();
                    
   IndicatorsData   *data[];

   void tick()
     {
      // Only continue with new bar
      if(IsNewBar() == false) return;

      // Create new data class
      IndicatorsData *newData=new IndicatorsData;

      // Fill the class with data
      newData.MADirection        = iCustom(NULL,0,"CombiEA/_MACross", 1, 2, 1);
      newData.MASwitch           = iCustom(NULL,0,"CombiEA/_MACross", 0, 4, 0);
      newData.BollingerPosition  = iCustom(NULL,0,"CombiEA/_BollingerIndicator",0,3,1);
      ///newData.TrendUp            = iCustom(NULL,0,"TL_by_Demark_v6",11,12,112);
      //newData.TrendDown          = iCustom(NULL,0,"TL_by_Demark_v6",121,8,112);
      newData.SarPoint           = iCustom(NULL,0,"CombiEA/_SAR",0,1,1);
      newData.AlligatorLips      = iCustom(NULL,0,"CombiEA/_Alligator",0,0,1);
      newData.AlligatorTeeth     = iCustom(NULL,0,"CombiEA/_Alligator",0,1,1);
      newData.AlligatorJaws      = iCustom(NULL,0,"CombiEA/_Alligator",0,2,1);
      
      //Print(!!(1.0));

      //Print(iCustom(NULL,0,"CombiEA/_MACross", 1 ,4, 1));        
      if (newData.MASwitch != 0.0) Print("is switch =" , newData.MASwitch);
      // Store the data
      storeData(newData);
      //Print(typeid(newData.MASwitch
      return;
     }

   IndicatorsData *getData()
     {
      return data[ArraySize(data)-1];
     }

  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

IndicatorFetcher::IndicatorFetcher()
  {
  }
  
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

IndicatorFetcher::~IndicatorFetcher()
  {
   int size=ArraySize(data);
   for(int i=0;i<size;i++)
      delete(data[i]);
  }
//+------------------------------------------------------------------+

datetime lastBarOpenTime;
bool IsNewBar()
{
   datetime thisBarOpenTime = Time[0];
   if(thisBarOpenTime != lastBarOpenTime)
   {
      lastBarOpenTime = thisBarOpenTime;
      return (true);
   }
   else
      return (false);
}
//+------------------------------------------------------------------+
