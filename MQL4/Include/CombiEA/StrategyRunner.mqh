//+------------------------------------------------------------------+
//|                                                   MACrossing.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict

#include ".\Util\Order.mqh"
#include ".\IndicatorFetcher.mqh"
#include ".\Strategies\BaseStrategy.mqh"
#include ".\Strategies\RSIStrategy.mqh"
#include ".\Strategies\MACrossing.mqh"
#include ".\Strategies\Sar.mqh"


//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class StrategyRunner
  {
private:
   BaseStrategy     *c_Strategies[];
   IndicatorFetcher *c_iData;
   UtilOrder        *c_UtilOrder;

   // Strategies
   BaseStrategy     *c_MACrossing;
   SarStrategy      *c_SarStrategy;
   RSIStrategy      *c_RSIStrategy;
public:
                     StrategyRunner(IndicatorFetcher *iData,UtilOrder *utilOrder);
                    ~StrategyRunner();

   void tick()
     {
         c_MACrossing.tick();
         //c_SarStrategy.tick();
         //c_RSIStrategy.tick();
     }
  };
  
  
  
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StrategyRunner::StrategyRunner(IndicatorFetcher *iData,UtilOrder *utilOrder):c_iData(iData), c_UtilOrder(utilOrder)
  {

// Load the strategies
   c_MACrossing   =new MACrossing(iData,utilOrder);
   c_SarStrategy  =new SarStrategy(iData,utilOrder);
   c_RSIStrategy  =new RSIStrategy(iData,utilOrder);
  }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
StrategyRunner::~StrategyRunner()
  {
   delete c_iData;
   delete c_UtilOrder;
   delete c_MACrossing;
   delete c_RSIStrategy;
  }
//+------------------------------------------------------------------+
